## tmux
cp .tmux.conf ~/

## vim
# Confirm vim supports python
# Install pathogen
mkdir -p ~/.vim/autoload ~/.vim/bundle && curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim
# Install python-mode
mkdir -p ~/.vim/bundle  && git clone --recurse-submodules https://github.com/python-mode/python-mode.git ~/.vim/bundle
# Update vim
cp .vimrc ~/
