""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"" Vundle
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set nocompatible            
filetype off                

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'gmarik/Vundle.vim'
Plugin 'tmhedberg/SimpylFold'
Plugin 'vim-scripts/indentpython.vim'
Bundle 'Valloric/YouCompleteMe'
Plugin 'vim-syntastic/syntastic'
Plugin 'nvie/vim-flake8'
Plugin 'jnurmine/Zenburn'
Plugin 'altercation/vim-colors-solarized'
Plugin 'scrooloose/nerdtree'
Plugin 'jistr/vim-nerdtree-tabs'
Plugin 'kien/ctrlp.vim'
Plugin 'tpope/vim-fugitive'
Plugin 'Lokaltog/powerline', {'rtp': 'powerline/bindings/vim/'}

" All of your Plugins must be added before the following line
call vundle#end()            
filetype plugin indent on    

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"" Split
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
set splitbelow
set splitright

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"" Folding
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set foldmethod=indent
set foldlevel=99
" Enable folding with the spacebar
nnoremap <space> za

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"" Python
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"" PEP8
au BufNewFile,BufRead *.py
    \set tabstop=4
    \set softtabstop=4
    \set shiftwidth=4
    \set textwidth=79
    \set expandtab
    \set autoindent
    \set fileformat=unix

"" Flag unnecessary whitespace

"" Use UTF-8
set encoding=utf-8

"" Close YouCompleteMe when done with it
let g:ycm_autoclose_preview_window_after_completion=1

"" Shortcut for goto definition
map <leader>g  :YcmCompleter GoToDefinitionElseDeclaration<CR>

"" Syntax highlighting
let python_highlight_all=1
syntax on

"" Hide .pyc files in NerdTree
let NERDTreeIgnore=['\.pyc$', '\~$'] "ignore files in NERDTree

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"" Asthetics
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
if has('gui_running')
  set background=dark
  colorscheme solarized
else
  colorscheme zenburn
endif

" Toggle between color schemes with F5
call togglebg#map("<F5>")

" Relative line numbering
set rnu

" System clipboard
set clipboard=unnamed

